### What

A Docker image for building 50/50 Blockchain repo.

### How to use

- git clone https://bitbucket.org/noahzhang/chain.build.env.git
- cd chain.build.env
- docker build -f Dockerfile.chain.dev -t chain.dev .
- docker run -it -d -p22:22 --name chain.dev chain.dev
- ssh root@localhost -p 22 **password: screencast**

- git clone https://bitbucket.org/exeblock/peerplays-chain.git
- cd peerplays
- git submodule update --init --recursive

- cd peerplays
- cmake -DBOOST_ROOT="$BOOST_ROOT" -DCMAKE_BUILD_TYPE=Release .
- make